// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Pacman/InkyController.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeInkyController() {}
// Cross Module References
	PACMAN_API UClass* Z_Construct_UClass_AInkyController_NoRegister();
	PACMAN_API UClass* Z_Construct_UClass_AInkyController();
	AIMODULE_API UClass* Z_Construct_UClass_AAIController();
	UPackage* Z_Construct_UPackage__Script_Pacman();
// End Cross Module References
	void AInkyController::StaticRegisterNativesAInkyController()
	{
	}
	UClass* Z_Construct_UClass_AInkyController_NoRegister()
	{
		return AInkyController::StaticClass();
	}
	struct Z_Construct_UClass_AInkyController_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_LocationToGo_MetaData[];
#endif
		static const UECodeGen_Private::FNamePropertyParams NewProp_LocationToGo;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AInkyController_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AAIController,
		(UObject* (*)())Z_Construct_UPackage__Script_Pacman,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AInkyController_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "InkyController.h" },
		{ "ModuleRelativePath", "InkyController.h" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AInkyController_Statics::NewProp_LocationToGo_MetaData[] = {
		{ "Category", "BB Key" },
		{ "Comment", "//BB key\n" },
		{ "ModuleRelativePath", "InkyController.h" },
		{ "ToolTip", "BB key" },
	};
#endif
	const UECodeGen_Private::FNamePropertyParams Z_Construct_UClass_AInkyController_Statics::NewProp_LocationToGo = { "LocationToGo", nullptr, (EPropertyFlags)0x0040000000010001, UECodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AInkyController, LocationToGo), METADATA_PARAMS(Z_Construct_UClass_AInkyController_Statics::NewProp_LocationToGo_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AInkyController_Statics::NewProp_LocationToGo_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AInkyController_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AInkyController_Statics::NewProp_LocationToGo,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AInkyController_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AInkyController>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_AInkyController_Statics::ClassParams = {
		&AInkyController::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AInkyController_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_AInkyController_Statics::PropPointers),
		0,
		0x009002A4u,
		METADATA_PARAMS(Z_Construct_UClass_AInkyController_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AInkyController_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AInkyController()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UECodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AInkyController_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AInkyController, 4042142104);
	template<> PACMAN_API UClass* StaticClass<AInkyController>()
	{
		return AInkyController::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AInkyController(Z_Construct_UClass_AInkyController, &AInkyController::StaticClass, TEXT("/Script/Pacman"), TEXT("AInkyController"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AInkyController);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
