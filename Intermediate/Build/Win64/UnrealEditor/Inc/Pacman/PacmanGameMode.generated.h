// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PACMAN_PacmanGameMode_generated_h
#error "PacmanGameMode.generated.h already included, missing '#pragma once' in PacmanGameMode.h"
#endif
#define PACMAN_PacmanGameMode_generated_h

#define Pacman_5_0_Source_Pacman_PacmanGameMode_h_12_SPARSE_DATA
#define Pacman_5_0_Source_Pacman_PacmanGameMode_h_12_RPC_WRAPPERS
#define Pacman_5_0_Source_Pacman_PacmanGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Pacman_5_0_Source_Pacman_PacmanGameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPacmanGameMode(); \
	friend struct Z_Construct_UClass_APacmanGameMode_Statics; \
public: \
	DECLARE_CLASS(APacmanGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Pacman"), PACMAN_API) \
	DECLARE_SERIALIZER(APacmanGameMode)


#define Pacman_5_0_Source_Pacman_PacmanGameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAPacmanGameMode(); \
	friend struct Z_Construct_UClass_APacmanGameMode_Statics; \
public: \
	DECLARE_CLASS(APacmanGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Pacman"), PACMAN_API) \
	DECLARE_SERIALIZER(APacmanGameMode)


#define Pacman_5_0_Source_Pacman_PacmanGameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	PACMAN_API APacmanGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APacmanGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(PACMAN_API, APacmanGameMode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APacmanGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	PACMAN_API APacmanGameMode(APacmanGameMode&&); \
	PACMAN_API APacmanGameMode(const APacmanGameMode&); \
public:


#define Pacman_5_0_Source_Pacman_PacmanGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	PACMAN_API APacmanGameMode(APacmanGameMode&&); \
	PACMAN_API APacmanGameMode(const APacmanGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(PACMAN_API, APacmanGameMode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APacmanGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APacmanGameMode)


#define Pacman_5_0_Source_Pacman_PacmanGameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define Pacman_5_0_Source_Pacman_PacmanGameMode_h_9_PROLOG
#define Pacman_5_0_Source_Pacman_PacmanGameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Pacman_5_0_Source_Pacman_PacmanGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	Pacman_5_0_Source_Pacman_PacmanGameMode_h_12_SPARSE_DATA \
	Pacman_5_0_Source_Pacman_PacmanGameMode_h_12_RPC_WRAPPERS \
	Pacman_5_0_Source_Pacman_PacmanGameMode_h_12_INCLASS \
	Pacman_5_0_Source_Pacman_PacmanGameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Pacman_5_0_Source_Pacman_PacmanGameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Pacman_5_0_Source_Pacman_PacmanGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	Pacman_5_0_Source_Pacman_PacmanGameMode_h_12_SPARSE_DATA \
	Pacman_5_0_Source_Pacman_PacmanGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Pacman_5_0_Source_Pacman_PacmanGameMode_h_12_INCLASS_NO_PURE_DECLS \
	Pacman_5_0_Source_Pacman_PacmanGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PACMAN_API UClass* StaticClass<class APacmanGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Pacman_5_0_Source_Pacman_PacmanGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
