// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PACMAN_PacmanCharacter_generated_h
#error "PacmanCharacter.generated.h already included, missing '#pragma once' in PacmanCharacter.h"
#endif
#define PACMAN_PacmanCharacter_generated_h

#define Pacman_5_0_Source_Pacman_PacmanCharacter_h_12_SPARSE_DATA
#define Pacman_5_0_Source_Pacman_PacmanCharacter_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetNbCoin);


#define Pacman_5_0_Source_Pacman_PacmanCharacter_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetNbCoin);


#define Pacman_5_0_Source_Pacman_PacmanCharacter_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPacmanCharacter(); \
	friend struct Z_Construct_UClass_APacmanCharacter_Statics; \
public: \
	DECLARE_CLASS(APacmanCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Pacman"), NO_API) \
	DECLARE_SERIALIZER(APacmanCharacter)


#define Pacman_5_0_Source_Pacman_PacmanCharacter_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAPacmanCharacter(); \
	friend struct Z_Construct_UClass_APacmanCharacter_Statics; \
public: \
	DECLARE_CLASS(APacmanCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Pacman"), NO_API) \
	DECLARE_SERIALIZER(APacmanCharacter)


#define Pacman_5_0_Source_Pacman_PacmanCharacter_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APacmanCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APacmanCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APacmanCharacter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APacmanCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APacmanCharacter(APacmanCharacter&&); \
	NO_API APacmanCharacter(const APacmanCharacter&); \
public:


#define Pacman_5_0_Source_Pacman_PacmanCharacter_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APacmanCharacter(APacmanCharacter&&); \
	NO_API APacmanCharacter(const APacmanCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APacmanCharacter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APacmanCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APacmanCharacter)


#define Pacman_5_0_Source_Pacman_PacmanCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CameraBoom() { return STRUCT_OFFSET(APacmanCharacter, CameraBoom); } \
	FORCEINLINE static uint32 __PPO__FollowCamera() { return STRUCT_OFFSET(APacmanCharacter, FollowCamera); }


#define Pacman_5_0_Source_Pacman_PacmanCharacter_h_9_PROLOG
#define Pacman_5_0_Source_Pacman_PacmanCharacter_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Pacman_5_0_Source_Pacman_PacmanCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	Pacman_5_0_Source_Pacman_PacmanCharacter_h_12_SPARSE_DATA \
	Pacman_5_0_Source_Pacman_PacmanCharacter_h_12_RPC_WRAPPERS \
	Pacman_5_0_Source_Pacman_PacmanCharacter_h_12_INCLASS \
	Pacman_5_0_Source_Pacman_PacmanCharacter_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Pacman_5_0_Source_Pacman_PacmanCharacter_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Pacman_5_0_Source_Pacman_PacmanCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	Pacman_5_0_Source_Pacman_PacmanCharacter_h_12_SPARSE_DATA \
	Pacman_5_0_Source_Pacman_PacmanCharacter_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Pacman_5_0_Source_Pacman_PacmanCharacter_h_12_INCLASS_NO_PURE_DECLS \
	Pacman_5_0_Source_Pacman_PacmanCharacter_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PACMAN_API UClass* StaticClass<class APacmanCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Pacman_5_0_Source_Pacman_PacmanCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
