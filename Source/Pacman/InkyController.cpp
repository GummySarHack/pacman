// Fill out your copyright notice in the Description page of Project Settings.


#include "InkyController.h"

#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Inky.h"

AInkyController::AInkyController()
{
	bTC = CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("BehaviourTreeComponent"));
	bBC = CreateDefaultSubobject<UBlackboardComponent>(TEXT("BlackBoardComponent"));
}

void AInkyController::OnPossess(APawn* pawn)
{
	Super::OnPossess(pawn);

	//check if actor valid
	AInky* inky = Cast<AInky>(pawn);

	if (inky)
	{
		//initialise blackboard
		if (inky->behaviourTree->BlackboardAsset)
		{
			bBC->InitializeBlackboard(*(inky->behaviourTree->BlackboardAsset));
		}
	}
	//set actor with behaviour tree
	bTC->StartTree(*inky->behaviourTree);
}