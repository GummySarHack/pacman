// Fill out your copyright notice in the Description page of Project Settings.


#include "Pinky.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/InputSettings.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "MotionControllerComponent.h"
#include "XRMotionControllerBase.h"
#include "MotionControllerComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BehaviorTree.h"

#include "Runtime/Engine/Classes/Components/BoxComponent.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Engine/World.h"
#include "Engine/Engine.h"
#include "Runtime/Engine/Classes/Components/BoxComponent.h"

// Sets default values
APinky::APinky()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	boxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollisionDamage"));


	blackBoardComp = CreateDefaultSubobject<UBlackboardComponent>(TEXT("blackBoardComp"));
	behaviourComp = CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("behaviourComp"));
}

// Called when the game starts or when spawned
void APinky::BeginPlay()
{
	Super::BeginPlay();
	boxComponent->OnComponentBeginOverlap.AddDynamic(this, &APinky::OnBoxBeginOverlap);
	pacmanPlayer = Cast<APacmanCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
}

// Called every frame
void APinky::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APinky::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void APinky::OnBoxBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, 
							   UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, 
							   bool bFromSweep, const FHitResult& SweepResult)
{
	if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Purple, TEXT("Overlap PINKY"));
	}

	auto pacmanPlayerActor = Cast<AActor>(pacmanPlayer);

	if (pacmanPlayerActor == OtherActor)
	{
		if (GEngine)
		{
			GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Purple, TEXT("PINKY OVERLAPPS ACTOR"));
		}
		pacmanPlayerActor->Destroy();
	}
}

void APinky::UpdateSpeed(float newSpeed)
{
	UCharacterMovementComponent* movementPtr = Cast<UCharacterMovementComponent>(this->GetCharacterMovement());
	movementPtr->MaxWalkSpeed = newSpeed;
}

