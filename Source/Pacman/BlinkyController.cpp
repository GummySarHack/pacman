// Fill out your copyright notice in the Description page of Project Settings.

#include "BlinkyController.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"

#include "Blinky.h"

ABlinkyController::ABlinkyController()
{
	bTC = CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("BehaviourTreeComponent"));
	bBC = CreateDefaultSubobject<UBlackboardComponent>(TEXT("BlackBoardComponent"));
}

void ABlinkyController::OnPossess(APawn* pawn)
{
	Super::OnPossess(pawn);

	//check if actor valid
	ABlinky* blinky = Cast<ABlinky>(pawn);

	if (blinky)
	{
		//initialise blackboard
		if (blinky->behaviourTree->BlackboardAsset)
		{
			bBC->InitializeBlackboard(*(blinky->behaviourTree->BlackboardAsset));
		}
	}
	//set actor with behaviour tree
	bTC->StartTree(*blinky->behaviourTree);
}

