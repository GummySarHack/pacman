// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"

#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BehaviorTree.h"

#include "BlinkyController.generated.h"

/**
 * 
 */
UCLASS()
class PACMAN_API ABlinkyController : public AAIController
{
	GENERATED_BODY()

private:

	//Behaviour Tree reference
	UBehaviorTreeComponent* bTC;

	//BB reference
	UBlackboardComponent* bBC;

	//BB key
	UPROPERTY(EditDefaultsOnly, Category = "BB Key")
		FName LocationToGo;

	//save my targets
	TArray<AActor*> targetPoints;

	virtual void OnPossess(APawn* pawn) override;

public:

	ABlinkyController();

	FORCEINLINE UBlackboardComponent* GetBlackboardComp() const { return bBC; }

};
