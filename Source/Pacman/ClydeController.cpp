// Fill out your copyright notice in the Description page of Project Settings.


#include "ClydeController.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Clyde.h"

AClydeController::AClydeController()
{
	bTC = CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("BehaviourTreeComponent"));
	bBC = CreateDefaultSubobject<UBlackboardComponent>(TEXT("BlackBoardComponent"));
}

void AClydeController::OnPossess(APawn* pawn)
{
	Super::OnPossess(pawn);

	//check if actor valid
	AClyde* clyde = Cast<AClyde>(pawn);

	if (clyde)
	{
		//initialise blackboard
		if (clyde->behaviourTree->BlackboardAsset)
		{
			bBC->InitializeBlackboard(*(clyde->behaviourTree->BlackboardAsset));
		}
	}
	//set actor with behaviour tree
	bTC->StartTree(*clyde->behaviourTree);
}

