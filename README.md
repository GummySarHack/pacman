This a little 3D Pacman game

Character Pacman was created on Blender through a tutorial on Youtube 
I haven't had the chance to create the ghosts yet, so for now, there're lovely coloured triangles :)

I enjoy working with AIs so I use Unreal Engine's Behaviour Trees to work with my ghosts
As of now, only Blinky is completed.
It is not my intention to complet a full Pacman game, I want to practice AIs and Behaviour Trees with this project.

I also wanted to try to learn Blender and a little animated Pacman was a fairly good start, not perfect, but I had much fun.
