// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FHitResult;
#ifdef PACMAN_Pinky_generated_h
#error "Pinky.generated.h already included, missing '#pragma once' in Pinky.h"
#endif
#define PACMAN_Pinky_generated_h

#define Pacman_5_0_Source_Pacman_Pinky_h_16_SPARSE_DATA
#define Pacman_5_0_Source_Pacman_Pinky_h_16_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execUpdateSpeed); \
	DECLARE_FUNCTION(execOnBoxBeginOverlap);


#define Pacman_5_0_Source_Pacman_Pinky_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execUpdateSpeed); \
	DECLARE_FUNCTION(execOnBoxBeginOverlap);


#define Pacman_5_0_Source_Pacman_Pinky_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPinky(); \
	friend struct Z_Construct_UClass_APinky_Statics; \
public: \
	DECLARE_CLASS(APinky, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Pacman"), NO_API) \
	DECLARE_SERIALIZER(APinky)


#define Pacman_5_0_Source_Pacman_Pinky_h_16_INCLASS \
private: \
	static void StaticRegisterNativesAPinky(); \
	friend struct Z_Construct_UClass_APinky_Statics; \
public: \
	DECLARE_CLASS(APinky, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Pacman"), NO_API) \
	DECLARE_SERIALIZER(APinky)


#define Pacman_5_0_Source_Pacman_Pinky_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APinky(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APinky) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APinky); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APinky); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APinky(APinky&&); \
	NO_API APinky(const APinky&); \
public:


#define Pacman_5_0_Source_Pacman_Pinky_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APinky(APinky&&); \
	NO_API APinky(const APinky&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APinky); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APinky); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APinky)


#define Pacman_5_0_Source_Pacman_Pinky_h_16_PRIVATE_PROPERTY_OFFSET
#define Pacman_5_0_Source_Pacman_Pinky_h_13_PROLOG
#define Pacman_5_0_Source_Pacman_Pinky_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Pacman_5_0_Source_Pacman_Pinky_h_16_PRIVATE_PROPERTY_OFFSET \
	Pacman_5_0_Source_Pacman_Pinky_h_16_SPARSE_DATA \
	Pacman_5_0_Source_Pacman_Pinky_h_16_RPC_WRAPPERS \
	Pacman_5_0_Source_Pacman_Pinky_h_16_INCLASS \
	Pacman_5_0_Source_Pacman_Pinky_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Pacman_5_0_Source_Pacman_Pinky_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Pacman_5_0_Source_Pacman_Pinky_h_16_PRIVATE_PROPERTY_OFFSET \
	Pacman_5_0_Source_Pacman_Pinky_h_16_SPARSE_DATA \
	Pacman_5_0_Source_Pacman_Pinky_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Pacman_5_0_Source_Pacman_Pinky_h_16_INCLASS_NO_PURE_DECLS \
	Pacman_5_0_Source_Pacman_Pinky_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PACMAN_API UClass* StaticClass<class APinky>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Pacman_5_0_Source_Pacman_Pinky_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
