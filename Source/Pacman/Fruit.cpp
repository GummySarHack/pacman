// Fill out your copyright notice in the Description page of Project Settings.


#include "Fruit.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Engine/World.h"
#include "Engine/Engine.h"
#include "Runtime/Engine/Classes/Components/BoxComponent.h"


// Sets default values
AFruit::AFruit()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	boxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollisionDamage"));
}

// Called when the game starts or when spawned
void AFruit::BeginPlay()
{
	Super::BeginPlay();

	boxComponent->OnComponentBeginOverlap.AddDynamic(this, &AFruit::OnBoxBeginOverlap);
	pacmanPlayer = Cast<APacmanCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
}

// Called every frame
void AFruit::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFruit::OnBoxBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, 
							UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, 
							bool bFromSweep, const FHitResult& SweepResult)
{
	if (pacmanPlayer == OtherActor)
	{
		AddCoins();
		/*if (auto gameInstanceCoin = Cast<UMyGameInstance>(GetGameInstance()))
		{
			gameInstanceCoin->takenCoinPositions.Add(GetActorLocation());
		}*/
	}
}

void AFruit::AddCoins()
{
	int addedCoin;

	if (pacmanPlayer)
	{
		addedCoin = pacmanPlayer->GetNbCoin();
		addedCoin = addedCoin + fruitWorth;
		pacmanPlayer->SetNbCoins(addedCoin);

		Destroy();
	}
}
