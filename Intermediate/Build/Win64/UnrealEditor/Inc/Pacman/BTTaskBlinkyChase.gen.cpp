// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Pacman/BTTaskBlinkyChase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBTTaskBlinkyChase() {}
// Cross Module References
	PACMAN_API UClass* Z_Construct_UClass_UBTTaskBlinkyChase_NoRegister();
	PACMAN_API UClass* Z_Construct_UClass_UBTTaskBlinkyChase();
	AIMODULE_API UClass* Z_Construct_UClass_UBTTaskNode();
	UPackage* Z_Construct_UPackage__Script_Pacman();
// End Cross Module References
	void UBTTaskBlinkyChase::StaticRegisterNativesUBTTaskBlinkyChase()
	{
	}
	UClass* Z_Construct_UClass_UBTTaskBlinkyChase_NoRegister()
	{
		return UBTTaskBlinkyChase::StaticClass();
	}
	struct Z_Construct_UClass_UBTTaskBlinkyChase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UBTTaskBlinkyChase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBTTaskNode,
		(UObject* (*)())Z_Construct_UPackage__Script_Pacman,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBTTaskBlinkyChase_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "BTTaskBlinkyChase.h" },
		{ "ModuleRelativePath", "BTTaskBlinkyChase.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UBTTaskBlinkyChase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UBTTaskBlinkyChase>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UBTTaskBlinkyChase_Statics::ClassParams = {
		&UBTTaskBlinkyChase::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UBTTaskBlinkyChase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UBTTaskBlinkyChase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UBTTaskBlinkyChase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UECodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UBTTaskBlinkyChase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UBTTaskBlinkyChase, 586215214);
	template<> PACMAN_API UClass* StaticClass<UBTTaskBlinkyChase>()
	{
		return UBTTaskBlinkyChase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UBTTaskBlinkyChase(Z_Construct_UClass_UBTTaskBlinkyChase, &UBTTaskBlinkyChase::StaticClass, TEXT("/Script/Pacman"), TEXT("UBTTaskBlinkyChase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UBTTaskBlinkyChase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
