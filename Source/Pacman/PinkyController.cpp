// Fill out your copyright notice in the Description page of Project Settings.


#include "PinkyController.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Pinky.h"

APinkyController::APinkyController()
{
	bTC = CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("BehaviourTreeComponent"));
	bBC = CreateDefaultSubobject<UBlackboardComponent>(TEXT("BlackBoardComponent"));
}

void APinkyController::OnPossess(APawn* pawn)
{
	Super::OnPossess(pawn);

	//check if actor valid
	APinky* pinky = Cast<APinky>(pawn);

	if (pinky)
	{
		//initialise blackboard
		if (pinky->behaviourTree->BlackboardAsset)
		{
			bBC->InitializeBlackboard(*(pinky->behaviourTree->BlackboardAsset));
		}
	}
	//set actor with behaviour tree
	bTC->StartTree(*pinky->behaviourTree);
}

