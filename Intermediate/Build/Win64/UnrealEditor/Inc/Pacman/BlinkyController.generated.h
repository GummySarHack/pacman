// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PACMAN_BlinkyController_generated_h
#error "BlinkyController.generated.h already included, missing '#pragma once' in BlinkyController.h"
#endif
#define PACMAN_BlinkyController_generated_h

#define Pacman_5_0_Source_Pacman_BlinkyController_h_20_SPARSE_DATA
#define Pacman_5_0_Source_Pacman_BlinkyController_h_20_RPC_WRAPPERS
#define Pacman_5_0_Source_Pacman_BlinkyController_h_20_RPC_WRAPPERS_NO_PURE_DECLS
#define Pacman_5_0_Source_Pacman_BlinkyController_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABlinkyController(); \
	friend struct Z_Construct_UClass_ABlinkyController_Statics; \
public: \
	DECLARE_CLASS(ABlinkyController, AAIController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Pacman"), NO_API) \
	DECLARE_SERIALIZER(ABlinkyController)


#define Pacman_5_0_Source_Pacman_BlinkyController_h_20_INCLASS \
private: \
	static void StaticRegisterNativesABlinkyController(); \
	friend struct Z_Construct_UClass_ABlinkyController_Statics; \
public: \
	DECLARE_CLASS(ABlinkyController, AAIController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Pacman"), NO_API) \
	DECLARE_SERIALIZER(ABlinkyController)


#define Pacman_5_0_Source_Pacman_BlinkyController_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABlinkyController(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABlinkyController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABlinkyController); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABlinkyController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABlinkyController(ABlinkyController&&); \
	NO_API ABlinkyController(const ABlinkyController&); \
public:


#define Pacman_5_0_Source_Pacman_BlinkyController_h_20_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABlinkyController(ABlinkyController&&); \
	NO_API ABlinkyController(const ABlinkyController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABlinkyController); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABlinkyController); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ABlinkyController)


#define Pacman_5_0_Source_Pacman_BlinkyController_h_20_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__LocationToGo() { return STRUCT_OFFSET(ABlinkyController, LocationToGo); }


#define Pacman_5_0_Source_Pacman_BlinkyController_h_17_PROLOG
#define Pacman_5_0_Source_Pacman_BlinkyController_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Pacman_5_0_Source_Pacman_BlinkyController_h_20_PRIVATE_PROPERTY_OFFSET \
	Pacman_5_0_Source_Pacman_BlinkyController_h_20_SPARSE_DATA \
	Pacman_5_0_Source_Pacman_BlinkyController_h_20_RPC_WRAPPERS \
	Pacman_5_0_Source_Pacman_BlinkyController_h_20_INCLASS \
	Pacman_5_0_Source_Pacman_BlinkyController_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Pacman_5_0_Source_Pacman_BlinkyController_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Pacman_5_0_Source_Pacman_BlinkyController_h_20_PRIVATE_PROPERTY_OFFSET \
	Pacman_5_0_Source_Pacman_BlinkyController_h_20_SPARSE_DATA \
	Pacman_5_0_Source_Pacman_BlinkyController_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	Pacman_5_0_Source_Pacman_BlinkyController_h_20_INCLASS_NO_PURE_DECLS \
	Pacman_5_0_Source_Pacman_BlinkyController_h_20_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PACMAN_API UClass* StaticClass<class ABlinkyController>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Pacman_5_0_Source_Pacman_BlinkyController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
