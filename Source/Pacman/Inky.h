// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "PacmanCharacter.h"
#include "GameFramework/Character.h"
#include "Runtime/AIModule/Classes/Perception/PawnSensingComponent.h"
#include "BehaviorTree/BlackboardComponent.h"

#include "Inky.generated.h"


UCLASS()
class PACMAN_API AInky : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AInky();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(BlueprintReadWrite, Category = Actor, meta = (AllowPrivateAccess = "true"))
		class APacmanCharacter* pacmanPlayer;

	//BB reference
	UBlackboardComponent* blackBoardComp;
	//BHT reference
	UBehaviorTreeComponent* behaviourComp;

	//access BHT
	UPROPERTY(EditAnywhere, Category = "AI")
		class UBehaviorTree* behaviourTree;

	UFUNCTION()
		void OnBoxBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
			UPrimitiveComponent* OtherComp, int32 OtherBodyIndex,
			bool bFromSweep, const FHitResult& SweepResult);

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Actor, meta = (AllowPrivateAccess = "true"))
		class UBoxComponent* boxComponent;

	UFUNCTION(BlueprintCallable)
		void UpdateSpeed(float newSpeed);
};
