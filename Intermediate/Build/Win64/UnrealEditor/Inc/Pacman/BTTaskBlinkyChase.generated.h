// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PACMAN_BTTaskBlinkyChase_generated_h
#error "BTTaskBlinkyChase.generated.h already included, missing '#pragma once' in BTTaskBlinkyChase.h"
#endif
#define PACMAN_BTTaskBlinkyChase_generated_h

#define Pacman_5_0_Source_Pacman_BTTaskBlinkyChase_h_15_SPARSE_DATA
#define Pacman_5_0_Source_Pacman_BTTaskBlinkyChase_h_15_RPC_WRAPPERS
#define Pacman_5_0_Source_Pacman_BTTaskBlinkyChase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Pacman_5_0_Source_Pacman_BTTaskBlinkyChase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUBTTaskBlinkyChase(); \
	friend struct Z_Construct_UClass_UBTTaskBlinkyChase_Statics; \
public: \
	DECLARE_CLASS(UBTTaskBlinkyChase, UBTTaskNode, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Pacman"), NO_API) \
	DECLARE_SERIALIZER(UBTTaskBlinkyChase)


#define Pacman_5_0_Source_Pacman_BTTaskBlinkyChase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUBTTaskBlinkyChase(); \
	friend struct Z_Construct_UClass_UBTTaskBlinkyChase_Statics; \
public: \
	DECLARE_CLASS(UBTTaskBlinkyChase, UBTTaskNode, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Pacman"), NO_API) \
	DECLARE_SERIALIZER(UBTTaskBlinkyChase)


#define Pacman_5_0_Source_Pacman_BTTaskBlinkyChase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UBTTaskBlinkyChase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBTTaskBlinkyChase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBTTaskBlinkyChase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBTTaskBlinkyChase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBTTaskBlinkyChase(UBTTaskBlinkyChase&&); \
	NO_API UBTTaskBlinkyChase(const UBTTaskBlinkyChase&); \
public:


#define Pacman_5_0_Source_Pacman_BTTaskBlinkyChase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UBTTaskBlinkyChase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBTTaskBlinkyChase(UBTTaskBlinkyChase&&); \
	NO_API UBTTaskBlinkyChase(const UBTTaskBlinkyChase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBTTaskBlinkyChase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBTTaskBlinkyChase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBTTaskBlinkyChase)


#define Pacman_5_0_Source_Pacman_BTTaskBlinkyChase_h_15_PRIVATE_PROPERTY_OFFSET
#define Pacman_5_0_Source_Pacman_BTTaskBlinkyChase_h_12_PROLOG
#define Pacman_5_0_Source_Pacman_BTTaskBlinkyChase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Pacman_5_0_Source_Pacman_BTTaskBlinkyChase_h_15_PRIVATE_PROPERTY_OFFSET \
	Pacman_5_0_Source_Pacman_BTTaskBlinkyChase_h_15_SPARSE_DATA \
	Pacman_5_0_Source_Pacman_BTTaskBlinkyChase_h_15_RPC_WRAPPERS \
	Pacman_5_0_Source_Pacman_BTTaskBlinkyChase_h_15_INCLASS \
	Pacman_5_0_Source_Pacman_BTTaskBlinkyChase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Pacman_5_0_Source_Pacman_BTTaskBlinkyChase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Pacman_5_0_Source_Pacman_BTTaskBlinkyChase_h_15_PRIVATE_PROPERTY_OFFSET \
	Pacman_5_0_Source_Pacman_BTTaskBlinkyChase_h_15_SPARSE_DATA \
	Pacman_5_0_Source_Pacman_BTTaskBlinkyChase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Pacman_5_0_Source_Pacman_BTTaskBlinkyChase_h_15_INCLASS_NO_PURE_DECLS \
	Pacman_5_0_Source_Pacman_BTTaskBlinkyChase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PACMAN_API UClass* StaticClass<class UBTTaskBlinkyChase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Pacman_5_0_Source_Pacman_BTTaskBlinkyChase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
