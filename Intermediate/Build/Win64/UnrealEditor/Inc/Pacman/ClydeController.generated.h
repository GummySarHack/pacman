// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PACMAN_ClydeController_generated_h
#error "ClydeController.generated.h already included, missing '#pragma once' in ClydeController.h"
#endif
#define PACMAN_ClydeController_generated_h

#define Pacman_5_0_Source_Pacman_ClydeController_h_20_SPARSE_DATA
#define Pacman_5_0_Source_Pacman_ClydeController_h_20_RPC_WRAPPERS
#define Pacman_5_0_Source_Pacman_ClydeController_h_20_RPC_WRAPPERS_NO_PURE_DECLS
#define Pacman_5_0_Source_Pacman_ClydeController_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAClydeController(); \
	friend struct Z_Construct_UClass_AClydeController_Statics; \
public: \
	DECLARE_CLASS(AClydeController, AAIController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Pacman"), NO_API) \
	DECLARE_SERIALIZER(AClydeController)


#define Pacman_5_0_Source_Pacman_ClydeController_h_20_INCLASS \
private: \
	static void StaticRegisterNativesAClydeController(); \
	friend struct Z_Construct_UClass_AClydeController_Statics; \
public: \
	DECLARE_CLASS(AClydeController, AAIController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Pacman"), NO_API) \
	DECLARE_SERIALIZER(AClydeController)


#define Pacman_5_0_Source_Pacman_ClydeController_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AClydeController(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AClydeController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AClydeController); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AClydeController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AClydeController(AClydeController&&); \
	NO_API AClydeController(const AClydeController&); \
public:


#define Pacman_5_0_Source_Pacman_ClydeController_h_20_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AClydeController(AClydeController&&); \
	NO_API AClydeController(const AClydeController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AClydeController); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AClydeController); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AClydeController)


#define Pacman_5_0_Source_Pacman_ClydeController_h_20_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__LocationToGo() { return STRUCT_OFFSET(AClydeController, LocationToGo); }


#define Pacman_5_0_Source_Pacman_ClydeController_h_17_PROLOG
#define Pacman_5_0_Source_Pacman_ClydeController_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Pacman_5_0_Source_Pacman_ClydeController_h_20_PRIVATE_PROPERTY_OFFSET \
	Pacman_5_0_Source_Pacman_ClydeController_h_20_SPARSE_DATA \
	Pacman_5_0_Source_Pacman_ClydeController_h_20_RPC_WRAPPERS \
	Pacman_5_0_Source_Pacman_ClydeController_h_20_INCLASS \
	Pacman_5_0_Source_Pacman_ClydeController_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Pacman_5_0_Source_Pacman_ClydeController_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Pacman_5_0_Source_Pacman_ClydeController_h_20_PRIVATE_PROPERTY_OFFSET \
	Pacman_5_0_Source_Pacman_ClydeController_h_20_SPARSE_DATA \
	Pacman_5_0_Source_Pacman_ClydeController_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	Pacman_5_0_Source_Pacman_ClydeController_h_20_INCLASS_NO_PURE_DECLS \
	Pacman_5_0_Source_Pacman_ClydeController_h_20_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PACMAN_API UClass* StaticClass<class AClydeController>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Pacman_5_0_Source_Pacman_ClydeController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
